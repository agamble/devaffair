from django.shortcuts import render, redirect
from django.http import HttpResponse
from devaffair.functions import *
import random

def home(request):
	context = {}
	context['request'] = request
	uni_list = get_uni_tuples()
	context['uni_list'] = uni_list
	print uni_list
	return render(request, 'devaffair/index.html', context)

def handle403(request):
	context = {}
	return render(request, 'devaffair/403.html', context)

def logout(request):

	request.session.flush()
	context = {}
	context['error'] = "You have successfully logged out."
	return render(request, 'devaffair/index.html', context)