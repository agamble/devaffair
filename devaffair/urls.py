from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from devaffair import views
import settings

urlpatterns = patterns('',
	url(r'^$', views.home, name='home'),
    # Examples:
    # url(r'^$', 'devbears.views.home', name='home'),
    # url(r'^devbears/', include('devbears.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),

    # url(r'^clubs/', 'devbears.views.clubs', name='clubs')
    url(r'^admin/', include(admin.site.urls)),
    url(r'^logout/', views.logout, name='logout'),


    #this one must be last so that nothing matches as being a university
    url(r'(?P<university>\w+)/', include('university.urls', namespace='university')),

    
    )

handler403 = 'devaffair.views.handle403'