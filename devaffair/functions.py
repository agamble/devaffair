import bcrypt
from clubs.models import Club
from django.http import HttpResponse
from django.core.exceptions import PermissionDenied
from websocket import create_connection
from urllib import urlopen
from django.shortcuts import render, redirect
import json
import readline

acceptable_unis = [

'berkeley',
'ucl',
'stanford',
'imperial',
'georgetown',
'cambridge',

]

long_unis_dict = {
	'berkeley': 'University of California, Berkeley',
	'ucl': 'University College London',
	'stanford': 'Stanford University',
	'imperial': 'Imperial College London',
	'georgetown': 'Georgetown University',
	'cambridge': 'University of Cambridge'
}

def get_uni_tuples():

	array = []
	for n in range(0, len(acceptable_unis)):
		uni_tuple = ( acceptable_unis[n], long_unis_dict[acceptable_unis[n]] )
		array.append(uni_tuple)
	print array
	return array


def get_short_unis_list():
	return acceptable_unis

def get_long_unis_dict():
	return long_unis_dict

def set_session(request, context):

	context['request'] = request

	return context

def has_permission(request, university):
	if not request.session.get('usertoken'):
		return False

	usertoken = request.session['usertoken']

	club = Club.objects.filter(usertoken=usertoken).values()

	if not club:
		print "Someone tried to access a page with the usertoken " + usertoken + " and was unsuccessful."
		return False

	url_array = request.get_full_path().split("/")
	print url_array
	# check to see if user is accessing correct university
	if url_array[2]:
		print university
		print club[0]['university']

		if not club[0]['university'] == university:
			print 'string is not equal'
			return False
	return True

def login(request, club_form, university, context):


	# handle those who are already logged in
	if request.session.get('usertoken'):
		context['error'] = "You cannot register because you are already logged in."
		return render(request, 'clubs/club_login.html', context)

	# set values for club from database and club from form
	club_form_email = club_form['club_email']
	club_database = Club.objects.filter(email=club_form_email).values()

	# handle problem if no user was found by supplied email address
	if not club_database:
		context['error'] = "No club with this email address was found."
		return render(request, 'clubs/club_login.html', context)

	# extract the specific club from the array
	club = club_database[0]

	# if True, set session variables
	# else Password was incorrect, return render with error
	if password_matches_database(club_form, club_database[0]):
		request.session['usertoken'] = club['usertoken']
		request.session['name'] = club['name']
		request.session['type'] = 'club'
		request.session['id'] = club['id']
		request.session['university'] = club['university']
		print 'successful login'

		return redirect('university:university_index', university=club['university'])
	else:
		print "bad password match"
		context['error'] = "Password did not match the one on file."
		return render(request, 'clubs/club_login.html', context)


# if match then return array with True and usertoken
# else return False

# BCRYPT RETURNS STRING IN USERTOKEN.PASSWORDHASH form
def password_matches_database(club, club_database):
	print 
	form_password_usertoken_hash = hash_password(club['password'], club_database['usertoken'])
	form_password_hash = form_password_usertoken_hash

	database_password_hash = club_database['password']
	# compares database value with given value
	if form_password_hash == database_password_hash:
		return True
	else:
		return False

# pass the the password as one variable to return it hashed with the user token
# pass two variables and the function just returns the hashed password
def hash_password(password, usertoken):
	if usertoken == None:
		usertoken = bcrypt.gensalt()
		hashed_password = bcrypt.hashpw(password, usertoken)
		return [hashed_password, usertoken]
	else:
		hashed_password = bcrypt.hashpw(password, usertoken)
		return hashed_password

def set_context_university(context, university):
	context['university'] = university



def verify_not_duplicate(club, university):
	club_name = club['club_name']
	club_president = club['president']
	club_email = club['club_email']
	club_website = club['website']
	club_description = club['club_description']


	results = Club.objects.filter(university=university).values()

	for n in range (0, len(results)):
		if results[n]['website'] == club_website:
			return False
	else:
		return True

def verify_email_is_from_university(email, university):

	# determines whether the email of the entered club is correctly associated with the university

	email_hostname = email.split('@')[1].split('.')[0]
	print email_hostname
	if university == email_hostname:
		return True
	else:
		return False

# function iterates through data for possible errors
# returns an error string if there's a problem
# else returns None
def verify_club_data(club, university):
	if not verify_not_duplicate(club, university):
		return 'There is already a club registered under this URL'
	if not verify_email_is_from_university(club['club_email'], university):
		return 'The club email must be under a ' + university.capitalize() + ' email address.'
	else:
		return None

def write_club_to_database_and_return_id(club, university, password_array):
	club_name = club['club_name']
	club_president = club['president']
	club_email = club['club_email']
	club_website = club['website']
	club_description = club['club_description']

	club_password = password_array[0]
	club_usertoken = password_array[1]

	c = Club(name=club_name, 
		president=club_president, 
		website=club_website, 
		email=club_email,
		password=club_password, 
		club_description=club_description, 
		university=university,
		usertoken=club_usertoken)
	c.save()
	print c.website
	return [c.id, c.website]

class WebSocketError(Exception):
     def __init__(self, value):
         self.value = value
     def __str__(self):
        return repr(self.value)

def handshake(host, port):
	try:
		u = urlopen("http://%s:%s/socket.io/1/" % (host, port))
	except WebSocketError as e:
		print "We couldn't connect to the node server, is it running?"
		print e.value
		return (None, None, None)
	response = u.readline()

	(sid, hbtimeout, ctimeout, supported) = response.split(":")
	return (sid, hbtimeout, ctimeout)


def prepare_socket_io_url():
	SOCKET_IO_HOST = "127.0.0.1"
	SOCKET_IO_PORT = "8081"
	(sid, hbtimeout, ctimeout) = handshake(SOCKET_IO_HOST, SOCKET_IO_PORT) #handshaking according to socket.io spec.
	if not sid:
		return
	socket_io_url = 'ws://' + SOCKET_IO_HOST + ':' + SOCKET_IO_PORT + '/socket.io/1/websocket/' + sid
	return socket_io_url

def grab_website_image(id, website):
	url = prepare_socket_io_url()
	if not url:
		return
	ws = create_connection(url)

	socket_object = { "name": 'website', "args": [id, website], "id": id, "website": website }
	json_string = json.dumps(socket_object)
	print json_string
	ws.send("2:::")
	ws.send('5:1::' + json_string)
	ws.send("0:::")

	ws.close()
