from django.conf.urls import patterns, include, url
from university import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='university_index'),
	url(r'^clubs/', include('clubs.urls', namespace='clubs')),
	url(r'^devs/', include('devs.urls', namespace='devs')),
)
