# Create your views here.
from django.shortcuts import render, redirect
from django.http import Http404
from devaffair.functions import *

# DONT FORGET COMMAS



def index(request, university):

	context = {}
	short_unis_list = get_short_unis_list()
	long_unis_dict = get_long_unis_dict()

	for n in range (0 , len(short_unis_list)):
		if university == short_unis_list[n]:
			set_session(request, context)
			context['long_uni'] = long_unis_dict[university]
			set_context_university(context, university)
			return render(request, 'university/index.html', context)
	else:
		raise Http404

