from django.conf.urls import patterns, include, url
from berkeley import views


urlpatterns = patterns('', 
	url(r'^$', views.index, name='ucl_index'),
	url(r'^clubs/', include('clubs.urls')),
	url(r'^devs/', include('devs.urls')),
	)


