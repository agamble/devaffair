from django.conf.urls import patterns, url

from clubs import views

urlpatterns = patterns('', 
	url(r'^$', views.index, name='club_index'),
	url(r'^(?P<club_id>\d+)$', views.club_profile, name="club_profile"),
	url(r'^register$', views.club_register, name="club_register"),
	url(r'^page/(?P<page_number>\d+)$', views.club_list_page, name="club_list_page" ),
	url(r'^login$', views.club_login, name='club_login'),
)