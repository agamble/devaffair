import hashlib
from django.shortcuts import render, redirect
from django.http import HttpResponse
from clubs.models import Club
from clubs.forms import ClubRegisterForm, ClubLoginForm
from devaffair.functions import *

# Create your views here.
def index(request, university):
	context = {}

	if not has_permission(request, university):
		raise PermissionDenied
	set_session(request, context)
	set_context_university(context, university)

	results = Club.objects.filter(university=university)[0:10].values()
	results_length = len(results)

	context['clubs_data'] = results
	context['request'] = request

	return render(request, 'clubs/clubs.html', context)

def club_profile(request, university, club_id):
	club = Club.objects.get(id=int(club_id))
	context = {'name': club.name, 
			'president': club.president, 
			'website': club.website,
			'email': club.email,
			'club_id': club_id, 
			'description': club.club_description}
	set_context_university(context, university)

	if not has_permission(request, university):
		raise PermissionDenied
	set_session(request, context)
	grab_website_image(club_id, club.website)


	return render(request, 'clubs/club_profile.html', context)

def club_list_page(request, university, page_number):

	context = {}

	if not has_permission(request, university):
		raise PermissionDenied
	set_session(request, context)



	page_number = int(page_number)
	# send back to club homepage if pagenumber is one

	set_context_university(context, university)
	if page_number == 1:
		return render(request, 'clubs/clubs.html')
	min_limit = 10*pagenumber
	max_limit = 10+min_limit
	results = Club.objects.all()[min_limit:max_limit]
	if not results:
		return render(request, 'clubs/clubs.html', {'error': "There are no results this far forward."})


def club_register(request, university):


	context = {}

	set_context_university(context, university)

	if request.method == 'POST':
		form = ClubRegisterForm(request.POST)
		# checks if form is valid via native django method
		if form.is_valid():

			# check for other errors in form data
			context['error'] = verify_club_data(form.cleaned_data, university)

			if request.session.get('usertoken'):
				context['error'] = "You cannot register because you are already logged in."

			password_array = hash_password(form.cleaned_data['password'], None)


			# return filled in form with error message
			if context['error']:
				context["form"] = ClubRegisterForm(request.POST)
				print context['error']
				return render(request, 'clubs/club_register.html', context)

			club_website_id_array = write_club_to_database_and_return_id(form.cleaned_data, university, password_array)

			return redirect('university:clubs:club_index', university=university)

	# request was GET
	else:
		form = ClubRegisterForm()
		context['form'] = form 

	# return final render outside else statement in case of some error for default behaviour	
	return render(request, 'clubs/club_register.html',  context)

def club_login(request, university):
	context = {}
	set_context_university(context, university)

	print "club_login called"
	if request.method == 'POST':
		form = ClubLoginForm(request.POST)
		if form.is_valid():
			context['form'] = form
			return login(request, form.cleaned_data, university, context)
		else:
			print "form is not valid"
	else:
		print "get was called"
		form = ClubLoginForm()
		context['form'] = form
		return render(request, 'clubs/club_login.html', context)


