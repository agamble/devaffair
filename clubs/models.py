from django.db import models

# Create your models here.

class Club(models.Model):
	name = models.CharField(max_length=200)
	president = models.CharField(max_length=50)
	club_description = models.CharField(max_length=140)
	rating_sum = models.IntegerField(default=0)
	raters = models.IntegerField(default=0)
	website = models.URLField(max_length=200)
	email = models.EmailField(max_length=50)
	password = models.CharField(max_length=100)
	usertoken = models.CharField(max_length=100)
	university = models.CharField(max_length=100)
