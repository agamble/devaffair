from django import forms

class ClubRegisterForm(forms.Form):
	club_name = forms.CharField(max_length=200)
	president = forms.CharField(max_length=100)
	club_description = forms.CharField(max_length=140)
	club_email = forms.EmailField()
	password = forms.CharField(widget=forms.PasswordInput)
	website = forms.URLField()

class ClubLoginForm(forms.Form):
	club_email = forms.EmailField()
	password = forms.CharField(widget=forms.PasswordInput)
	remember = forms.BooleanField(required=False)