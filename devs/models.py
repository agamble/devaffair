from django.db import models

# Create your models here.
class Dev(models.Model):
	name = models.CharField(max_length=100)
	email = models.EmailField(max_length=50)
	password = models.CharField(max_length=100)
	dev_description = models.CharField(max_length=140)
	tags = models.CharField(max_length=200)
	facebook_page = models.URLField(max_length=200)
	university = models.CharField(max_length=100)
	prior_work = models.IntegerField()
	usertoken = models.CharField(max_length=50)


class PriorWork(models.Model):
	name = models.CharField(max_length=50)
	image = models.ImageField(upload_to='prior_work')
	work_description = models.CharField(max_length=140)
	rating_sum = models.IntegerField(default=0)
	raters = models.IntegerField(default=0)
	website = models.URLField()
	related_dev = models.IntegerField()

