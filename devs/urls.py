from django.conf.urls import patterns, include, url
from devs import views


urlpatterns = patterns('', 
	url(r'^$', views.index, name='dev_index'),
	url(r'^(?P<club_id>\d+)$', views.dev_profile, name="dev_profile"),
	url(r'^register$', views.dev_register, name="dev_register"),
	url(r'^page/(?P<page_number>\d+)$', views.dev_list, name="dev_list" )

	)


