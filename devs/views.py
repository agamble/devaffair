# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render, redirect

from django.conf.urls import patterns, include, url
from devs.models import Dev

def index(request, university):
	results = Dev.objects.filter(university=university)[0:10].values()
	results_length = len(results)


	web_tags = [
	"HTML/CSS",
	"Javascript/jQuery",
	"Wordpress Theming",
	"Dreamweaver",
	]

	design_tags = [
	"Photoshop",
	"Ilustrator",
	"InDesign",
	]

	programming_tags = [
	"Django",
	"Python",
	"PHP",
	"Ruby",
	"C/C++",
	"Node.js"
	"SQL Databases",
	"NoSQL Databases",
	"Objective-C",
	"Java"
	]




	context = {}
	context['web_tags'] = web_tags
	context['design_tags'] = design_tags
	context['programming_tags'] = programming_tags

	context['devs_data'] = results
	return render(request, 'clubs/clubs.html', context)


def dev_profile(request, university):
	return HttpResponse("yeah")

def dev_list(request, university):
	return HttpResponse('yeah')

def dev_register(request, university):
	if request.method == 'POST':
		form = DevRegisterForm(request.POST)
		# checks if form is valid via native django method
		precleaned_email = form.data['email']
		if form.is_valid() and verify_email(precleaned_email , university):
			name = form.cleaned_data['name']
			email = form.cleaned_data['email']
			dev_description = form.cleaned_data['dev_description']
			tags = form.cleaned_data['tags']
			facebook_page = form.cleaned_data['facebook_page']
			university = form.cleaned_data['university']
			# creates array of data
			# iterates through array to check each term exists
			# -1 corrects off by one bug
			d = Dev(name=name, 
				email=email, 
				dev_description=dev_description, 
				tags=tags, 
				facebook_page=facebook_page, 
				university=university)
			d.save()

			return redirect('club_index')
	# request was not made through post		
	else:
		form = DevRegisterForm()
		context = { "form": form }
	return render(request, 'devs/dev_register.html',  {"form" : form })