from django import forms

class DevRegisterForm(forms.Form):

	name = forms.CharField(max_length=100)
	email = forms.EmailField(max_length=50)
	password = forms.CharField(widget=forms.PasswordInput, max_length=50)
	dev_description = forms.CharField(max_length=140)
	tags = forms.CharField(max_length=200)
	facebook_page = models.URLField(max_length=200)



